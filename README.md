# Step Project "Cards"

## Technologies used
- [Gulp](https://gulpjs.com/)
- [SCSS](https://sass-lang.com/documentation/)
- [HTML5](https://developer.mozilla.org/ru/docs/Glossary/HTML5)
- [JavaScript](https://developer.mozilla.org/ru/docs/Web/JavaScript)

## Team Members
- Yaroslav Chaban
- Kostya Retyvykh
- Dmytro Podmazko


### Dmytro Podmazko
- header + login function

### Kostya Retyvykh
- create visit function

### Yaroslav Chaban
- display/edit/delete card and filters
